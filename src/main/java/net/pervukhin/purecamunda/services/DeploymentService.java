package net.pervukhin.purecamunda.services;

import net.pervukhin.purecamunda.util.IoUtil;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class DeploymentService {
    @Autowired
    private ProcessEngine processEngine;

    public void deploy(String fileName) {
        String xml = IoUtil.getResourceFileAsString(fileName);
        BpmnModelInstance modelInstance = Bpmn.readModelFromStream(
                new ByteArrayInputStream(xml.getBytes()));

        processEngine.getRepositoryService()
                .createDeployment()
                .addModelInstance(fileName, modelInstance)
                .deploy();
    }
}
