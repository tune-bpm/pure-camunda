package net.pervukhin.purecamunda;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableProcessApplication
public class PureCamundaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PureCamundaApplication.class, args);
    }

}
