package net.pervukhin.purecamunda.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@Component
public class RestDelegate implements JavaDelegate {
    private static final Logger logger = LoggerFactory.getLogger(RestDelegate.class);
    private static final Random random = new Random();

    @Autowired
    private RestTemplate restTemplate;

    @Value("${rest.request.url}")
    private String restRequestUrl;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        final String result = restTemplate.getForObject(restRequestUrl + "?v=" + random.nextLong(), String.class);
        logger.debug(result);
    }
}
